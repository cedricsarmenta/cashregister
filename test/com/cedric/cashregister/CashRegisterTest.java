package com.cedric.cashregister;

import java.util.Map;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class CashRegisterTest {
	
	private CashRegister cashRegister;
	
	private BillFactory billFactory;
	

	private Bill oneDollar;
	private Bill twoDollars;
	private Bill fiveDollars;
	private Bill tenDollars;
	private Bill twentyDollars;
	
	//TODO: complete unit tests
	@Before
	public void setUp() throws CashRegisterException {
		billFactory  = BillFactoryImpl.getInstance();
		//Initial contents
		cashRegister = new InMemoryCashRegister();
		oneDollar = billFactory.getBill(1);
		twoDollars = billFactory.getBill(2);
		fiveDollars = billFactory.getBill(5);
		tenDollars = billFactory.getBill(10);
		twentyDollars = billFactory.getBill(20);
		cashRegister.put(oneDollar, 5);
		cashRegister.put(twoDollars, 10);
		cashRegister.put(fiveDollars, 2);
		cashRegister.put(tenDollars, 1);
	}

	@Test
	public void getAllItems() {
		
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(5, contents.size());
		assertEquals(5, contents.get(oneDollar).intValue());
		assertEquals(10, contents.get(twoDollars).intValue());
		assertEquals(2, contents.get(fiveDollars).intValue());
		assertEquals(1, contents.get(tenDollars).intValue());
		assertEquals(0, contents.get(twentyDollars).intValue());
		
	}
	
	@Test
	public void testPut() throws CashRegisterException {
		cashRegister.put(tenDollars, 1);
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(2, contents.get(tenDollars).intValue());
	}
	@Test
	public void testTake() throws CashRegisterException {
		cashRegister.take(tenDollars, 1);
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(0, contents.get(tenDollars).intValue());
	}
	
	
	@Test
	public void testGetTotal() {
		assertEquals(45l, cashRegister.getTotalAmount().longValue());
	}
	
	@Test
	public void testChange() throws CashRegisterException {

		Map<Bill, Integer> change = cashRegister.change(23);
		assertEquals(5,change.size());

		assertEquals(1, change.get(oneDollar).intValue());
		assertEquals(1, change.get(twoDollars).intValue());
		assertEquals(2, change.get(fiveDollars).intValue());
		assertEquals(1, change.get(tenDollars).intValue());
		assertEquals(0, change.get(twentyDollars).intValue());
		
		
		
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(5, contents.size());
		assertEquals(4, contents.get(oneDollar).intValue());
		assertEquals(9, contents.get(twoDollars).intValue());
		assertEquals(0, contents.get(fiveDollars).intValue());
		assertEquals(0, contents.get(tenDollars).intValue());
		assertEquals(0, contents.get(twentyDollars).intValue());
		
	}
	
	
	@Test
	public void testChange2() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();
		cashRegister.put(twoDollars, 10);
		
		cashRegister.put(fiveDollars, 3);
		
		Map<Bill, Integer> change = cashRegister.change(16);
		assertEquals(5,change.size());

		
		
		
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(5, contents.size());
		assertEquals(0, change.get(oneDollar).intValue());
		assertEquals(3, change.get(twoDollars).intValue());
		assertEquals(2, change.get(fiveDollars).intValue());
		assertEquals(0, change.get(tenDollars).intValue());
		assertEquals(0, change.get(twentyDollars).intValue());
		
	}
	
	@Test
	public void testChange3() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();
		cashRegister.put(twoDollars, 10);
		
		cashRegister.put(fiveDollars, 1);
		
		Map<Bill, Integer> change = cashRegister.change(16);
		assertEquals(5,change.size());

		
		
		
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(5, contents.size());
		assertEquals(0, change.get(oneDollar).intValue());
		assertEquals(8, change.get(twoDollars).intValue());
		assertEquals(0, change.get(fiveDollars).intValue());
		assertEquals(0, change.get(tenDollars).intValue());
		assertEquals(0, change.get(twentyDollars).intValue());
		
	}
	
	@Test
	public void testChange4() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();
		cashRegister.put(twentyDollars, 1);
		
		cashRegister.put(tenDollars, 2);
		
		cashRegister.put(fiveDollars, 1);
	
		
		Map<Bill, Integer> change = cashRegister.change(25);
		assertEquals(5,change.size());

		
		
		
		Map<Bill, Integer> contents = cashRegister.getAllItems();
		assertEquals(5, contents.size());
		assertEquals(0, change.get(oneDollar).intValue());
		assertEquals(0, change.get(twoDollars).intValue());
		assertEquals(1, change.get(fiveDollars).intValue());
		assertEquals(0, change.get(tenDollars).intValue());
		assertEquals(1, change.get(twentyDollars).intValue());
		
	}
	
	@Test(expected=CashRegisterException.class)
	public void testChange5() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();

		
		cashRegister.put(twoDollars, 1);
		
		cashRegister.put(fiveDollars, 1);

		cashRegister.put(oneDollar, 1);
		
		cashRegister.change(4);
	
		
	}
	
	@Test(expected=CashRegisterException.class)
	public void testChange6() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();

		
		cashRegister.put(twoDollars, 1);
		
		cashRegister.put(fiveDollars, 1);

		cashRegister.put(oneDollar, 1);
		
		cashRegister.change(9);
	
		
	}
	

	@Test(expected=CashRegisterException.class)
	public void testChange7() throws CashRegisterException {
		cashRegister = new InMemoryCashRegister();


		cashRegister.put(twentyDollars, 1);
		cashRegister.put(tenDollars, 1);
		cashRegister.put(twoDollars, 1);
		
		cashRegister.put(fiveDollars, 1);

		cashRegister.put(oneDollar, 1);
		
		cashRegister.change(34);
	
		
	}
	
	
	@Test(expected=CashRegisterException.class)
	public void testChange8() throws CashRegisterException {

		cashRegister.change(999);
		

		
		
	}
}
