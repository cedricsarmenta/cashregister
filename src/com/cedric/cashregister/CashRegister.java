package com.cedric.cashregister;

import java.util.Map;

public interface CashRegister {
	Map<Bill, Integer> getAllItems();
	Long getTotalAmount();
	void put(Bill bill, int count) throws CashRegisterException;
	void take(Bill bill, int count) throws CashRegisterException;
	Map<Bill, Integer> change(Integer amount) throws CashRegisterException;
}
