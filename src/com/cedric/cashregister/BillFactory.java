package com.cedric.cashregister;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;

public interface BillFactory {

	Bill getBill(Integer denomination);
	
	//Returns sorted setd of bills in descending order
	List<Bill> getSortedSupportedBills();
}
