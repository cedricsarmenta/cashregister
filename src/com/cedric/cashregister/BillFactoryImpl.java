package com.cedric.cashregister;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class BillFactoryImpl implements BillFactory {
	//TODO: Have concept of currency? One factory of bills per currency?
	private  final Map<Integer, Bill> bills = new HashMap<>();
	
	private static BillFactory billFactory;
	
	public static BillFactory getInstance() {
		if(billFactory == null) {
			billFactory = new BillFactoryImpl();
		}
		
		return billFactory;
	}
	
	private BillFactoryImpl() {
		bills.put(1, new Bill(1));
		bills.put(2,new Bill(2));
		bills.put(5,new Bill(5));
		bills.put(10,new Bill(10));
		bills.put(20,new Bill(20));
	}

	
	//Flyweight pattern. Keep 1 memory instance per bill denomination
	@Override
	public  Bill getBill(Integer denomination) {
		if(!bills.containsKey(denomination)) {
			throw new IllegalArgumentException("Unsupported Bill");
		}
		
		return bills.get(denomination);
	}

	@Override
	public List<Bill> getSortedSupportedBills() {
		List<Bill> supportedBills = new ArrayList<>();
		supportedBills.addAll(bills.values());
		Collections.sort(supportedBills, InMemoryCashRegister.DESCENDING_BILL_COMPARATOR);
		
		return supportedBills;
	}
	
}
