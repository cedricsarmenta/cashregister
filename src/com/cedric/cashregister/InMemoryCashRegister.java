package com.cedric.cashregister;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class InMemoryCashRegister implements CashRegister {
	private Map<Bill, Integer> contents;
	
	//TODO: Extract as separate class
	public static final DescendingBillComparator DESCENDING_BILL_COMPARATOR = new DescendingBillComparator();
	
	private static class DescendingBillComparator implements Comparator<Bill> {

		@Override
		public int compare(Bill o1, Bill o2) {
			
			return o2.getDenomination().compareTo(o1.getDenomination());
		}
	};
	
	public InMemoryCashRegister() {
		 init();
	}
	
	private void init() {
		contents = new TreeMap<>(DESCENDING_BILL_COMPARATOR);
		initEmptyRegisterMap(contents);
	}

	private void initEmptyRegisterMap(Map<Bill, Integer> contents) {
		for(Bill bill : BillFactoryImpl.getInstance().getSortedSupportedBills()) {
			contents.put(bill, 0);
		}
	}
	@Override
	public Map<Bill, Integer> getAllItems() {
		return contents;
	}

	@Override
	public void put(Bill bill, int count) throws CashRegisterException {
		if(count < 0) {
			throw new CashRegisterException("Cannot put value less than 0");
		}
		int previousValue = contents.get(bill);
		contents.put(bill,previousValue + count);
	}

	@Override
	public void take(Bill bill, int count) throws CashRegisterException{
		if(count < 0) {
			throw new CashRegisterException("Cannot take value less than 0");
		}
		int previousValue = contents.get(bill);
		int newValue = previousValue - count;
		
		if(newValue < 0) {
			throw new CashRegisterException("Insufficient funds");
		}
		contents.put(bill,newValue);	
	}

	@Override
	public Map<Bill, Integer> change(Integer amount) throws CashRegisterException {
		if(amount <= 0 ) {
			throw new CashRegisterException("Invalid amount to change " + amount);
		}
		
		
		if(amount > this.getTotalAmount() ) {
			throw new CashRegisterException("Cannot change " + amount + ". Insufficient funds");
		}
		Map<Bill, Integer> changeBills = calculateChangeBills(amount);

		removeChangeFromRegister(changeBills);
		
		return changeBills;
		
	}

	private void removeChangeFromRegister(Map<Bill, Integer> changeBills) throws CashRegisterException {
		for(Map.Entry<Bill, Integer> entry : changeBills.entrySet()) {
			this.take(entry.getKey(), entry.getValue());
		}
	}

	private Map<Bill, Integer>  calculateChangeBills(Integer amount) throws CashRegisterException {
		Integer originalAmount = amount;
		Map<Bill, Integer> changeBills = new TreeMap<>(DESCENDING_BILL_COMPARATOR);

		
		initEmptyRegisterMap(changeBills);
		
	Bill fiveDollars = BillFactoryImpl.getInstance().getBill(5);

		int attempt = 0;
		do {
			amount = findChangeCombination(amount, changeBills, attempt++);
		} while(amount != 0 && changeBills.get(fiveDollars) > 0);
			
		
		if(amount == 0) {
			return changeBills;
		}
		
		
		throw new CashRegisterException("Cannot obtain change for " + originalAmount);
	}

	//Since five dollars cannot be exactly divided by 2 dollars,
	//It is possible to have combinations using 2 dollars while lessening/ignoring the five dollar amount
	//For example, to change for 8, we can either 5 + 3 + 1, or, 4 x 2
	//Because of this, we need to try all possible combinations using different amounts of 5 dollar bill solutions
	
	private Integer findChangeCombination(Integer amount, Map<Bill, Integer> changeBills, int attempt) {
		Bill fiveDollars = BillFactoryImpl.getInstance().getBill(5);

		
		for(Map.Entry<Bill, Integer> entry : contents.entrySet()) {
			Bill bill = entry.getKey();
			
			if(attempt > 0 && bill.getDenomination() > fiveDollars.getDenomination()) {
				continue;
			}
			
			Integer previousBillCount = changeBills.get(bill);
	
			if(fiveDollars.equals(bill) && attempt > 0) {
				//Backtrack one of the bills for five dollars
				changeBills.put(fiveDollars, previousBillCount - 1);
				amount += fiveDollars.getDenomination();
				continue;
				
			}

			
			//Adjust the smaller bills if the amount can now be computed
			Integer remainingBills = entry.getValue() - previousBillCount;
			
			if(remainingBills > 0 && bill.getDenomination() <= amount) {
		
				int takeableBills = amount / bill.getDenomination();
			
				int numberOfBills = Math.min(takeableBills, remainingBills);
				changeBills.put(bill, numberOfBills + previousBillCount);
				amount = amount - (numberOfBills * bill.getDenomination());
			} 
			
			if(amount == 0) {
				return amount;
			}
				
		}
		return amount;
	}

	@Override
	public Long getTotalAmount() {
		Long sum = 0l;
		for(Map.Entry<Bill, Integer> entry : contents.entrySet()) {
			sum += entry.getKey().getDenomination() * entry.getValue();
		}

		
		return sum;
	}

}
