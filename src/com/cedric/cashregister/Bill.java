package com.cedric.cashregister;

public class Bill {
	private Integer denomination;
	//TODO: Add currency?
	public Integer getDenomination() {
		return denomination;
	}
	

	Bill(Integer denomination) {
		super();
		this.denomination = denomination;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((denomination == null) ? 0 : denomination.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bill other = (Bill) obj;
		if (denomination == null) {
			if (other.denomination != null)
				return false;
		} else if (!denomination.equals(other.denomination))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Bill [denomination=" + denomination + "]";
	}


	
	
	
}
