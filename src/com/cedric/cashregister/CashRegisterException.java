package com.cedric.cashregister;

public class CashRegisterException extends Exception {
	
	public CashRegisterException(String message) {
		super(message);
	}

}
