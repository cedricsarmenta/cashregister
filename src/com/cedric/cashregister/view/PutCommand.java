package com.cedric.cashregister.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cedric.cashregister.Bill;
import com.cedric.cashregister.BillFactoryImpl;
import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;
import com.cedric.cashregister.InMemoryCashRegister;

public class PutCommand implements CashRegisterCommand {

	@Override
	public CommandResponse execute(String input, CashRegister cashRegister) {
		CommandResponse response = new CommandResponse();
		
		String[] parts = input.split(CashRegisteViewUtil.SPACE);
		
		if(parts.length != 6) {
			response.addError("Invalid command arguments for put");
			return response;
		}
		
		
		List<Integer> amounts = CashRegisteViewUtil.extractAmount(parts);
		
		Map<Bill, Integer> billAmountMap = CashRegisteViewUtil.extractAmountForBills(amounts);
		
		putBills(cashRegister, response, billAmountMap);
		
		response.appendOutput(CashRegisteViewUtil.displayRegisterState(cashRegister));
		return response;
	}

	private void putBills(CashRegister cashRegister, CommandResponse response, Map<Bill, Integer> billAmountMap) {
		for(Map.Entry<Bill, Integer> entry : billAmountMap.entrySet()) {
			response.appendOutput("Putting quantity: " + entry.getValue() + " for bill: " + entry.getKey() + "\n");
			try {
				cashRegister.put(entry.getKey(), entry.getValue());
				response.appendOutput("Success\n");
			} catch (CashRegisterException e) {
				response.addError("Put failed for bill: " + entry.getKey() + " Error: " + e.getMessage());
			}
		}
	}

	

}
