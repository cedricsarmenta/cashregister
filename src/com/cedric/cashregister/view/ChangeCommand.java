package com.cedric.cashregister.view;

import java.util.Map;

import com.cedric.cashregister.Bill;
import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;

public class ChangeCommand implements CashRegisterCommand {

	@Override
	public CommandResponse execute(String input, CashRegister cashRegister) {
		CommandResponse response = new CommandResponse();
		String[] parts = input.split(CashRegisteViewUtil.SPACE);
		
		if(parts.length != 2) {
			response.addError("Invalid command arguments for put");
			return response;
		}
		
		
		Integer amountToChange = Integer.parseInt(parts[1]);
		
		try {
			Map<Bill, Integer> changeMap  = cashRegister.change(amountToChange);
			response.appendOutput("Successfully changed: "+amountToChange+ " Your change: \n");
			response.appendOutput(CashRegisteViewUtil.printBillMap(changeMap));
		} catch (CashRegisterException e) {
			response.addError("Change failed for amount: " + amountToChange + " Error: " + e.getMessage());
		}
		
		
		return response;
	}

}
