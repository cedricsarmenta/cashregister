package com.cedric.cashregister.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cedric.cashregister.Bill;
import com.cedric.cashregister.BillFactoryImpl;
import com.cedric.cashregister.CashRegister;

public class CashRegisteViewUtil {
	public static final String SPACE = " ";

	public static String printAsCurrency(Long amount) {
		//TODO: Support currencies?
		return "$" + amount;
	}
	public static String displayRegisterState(CashRegister cashRegister) {
		Long total = cashRegister.getTotalAmount();

		String output = CashRegisteViewUtil.printAsCurrency(total) + SPACE;
		
		output+= printBillMap(cashRegister.getAllItems());
		return output;
	}
	public static String printBillMap(Map<Bill, Integer> billMap) {
		String output = "";
		for(Map.Entry<Bill, Integer> entry : billMap.entrySet()) {
			output += entry.getValue() + SPACE;
		}
		return output;
	}
	
	public static Map<Bill, Integer> extractAmountForBills(List<Integer> amounts) {
		Map<Bill, Integer> billAmountMap = new HashMap<>();
		
		int i=0;
		for(Bill bill : BillFactoryImpl.getInstance().getSortedSupportedBills()) {
			billAmountMap.put(bill, amounts.get(i++));
		}
		
		return billAmountMap;
	}

	public static List<Integer> extractAmount(String[] parts) {
		List<Integer> amounts = new ArrayList<>();
		
		for (int i = 1; i < parts.length ; i++) {
			amounts.add(Integer.parseInt(parts[i]));
		}
		
		return amounts;
	}

}
