package com.cedric.cashregister.view;

import java.util.Scanner;

import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;
import com.cedric.cashregister.InMemoryCashRegister;

public class CashRegisterView {
	private CashRegister cashRegister;

	public void init() {
		cashRegister = new InMemoryCashRegister();
		System.out.println("Ready...");
		
		Scanner scanner = new Scanner(System.in);
		
		while(true) {
			String input = getInput(scanner);
			try {
				CashRegisterCommand command = CommandFactory.getCommandFromInput(input);
				CommandResponse response = command.execute(input, cashRegister);
				
				if(response.getOutput() != null) {
					System.out.println("Output: ");
					System.out.println(response.getOutput());
				}
				
				if(response.getErrors().size() > 0) {
					System.out.println("Errors: ");
					
					for(String error : response.getErrors()) {
						System.out.println(error);
					}
				}
				
				if(response.isShouldTerminate()) {
					break;
				}
			} catch(Exception e) {
				//Generic handling
				System.out.println("An unexpected error occured: " + e.getMessage());
				e.printStackTrace();
			}
			
		}
		
	}

	private String getInput(Scanner scanner) {
		System.out.println(">");
		String input = scanner.nextLine();
		return input;
	}
	
	
}
