package com.cedric.cashregister.view;

import java.util.Map;

import com.cedric.cashregister.Bill;
import com.cedric.cashregister.CashRegister;

public class ShowCommand implements CashRegisterCommand {

	
	@Override
	public CommandResponse execute(String input, CashRegister cashRegister) {
		CommandResponse response = new CommandResponse();
		response.setOutput(CashRegisteViewUtil.displayRegisterState(cashRegister));

		return response;
		
	}

	

}
