package com.cedric.cashregister.view;

import java.util.ArrayList;
import java.util.List;

public class CommandResponse {
	private String output;
	private boolean shouldTerminate;
	private List<String> errors = new ArrayList<>();

	public String getOutput() {
		return output;
	}
	public void setOutput(String output) {
		this.output = output;
	}
	
	public void appendOutput(String output) {
		if(this.output == null) {
			this.output = "";
		}
		this.output += output;
	}
	public List<String> getErrors() {
		return errors;
	}
	
	public void addError(String error) {
		errors.add(error);
	}
	public boolean isShouldTerminate() {
		return shouldTerminate;
	}
	public void setShouldTerminate(boolean shouldTerminate) {
		this.shouldTerminate = shouldTerminate;
	}
	
	
} 
