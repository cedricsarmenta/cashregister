package com.cedric.cashregister.view;

import java.util.List;
import java.util.Map;

import com.cedric.cashregister.Bill;
import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;

public class TakeCommand implements CashRegisterCommand {


	@Override
	public CommandResponse execute(String input, CashRegister cashRegister) {
		CommandResponse response = new CommandResponse();
		
		String[] parts = input.split(CashRegisteViewUtil.SPACE);
		
		if(parts.length != 6) {
			response.addError("Invalid command arguments for take");
			return response;
		}
		
		
		List<Integer> amounts = CashRegisteViewUtil.extractAmount(parts);
		
		Map<Bill, Integer> billAmountMap = CashRegisteViewUtil.extractAmountForBills(amounts);
		
		takeBills(cashRegister, response, billAmountMap);
		
		response.appendOutput(CashRegisteViewUtil.displayRegisterState(cashRegister));
		return response;
	}

	private void takeBills(CashRegister cashRegister, CommandResponse response, Map<Bill, Integer> billAmountMap) {
		for(Map.Entry<Bill, Integer> entry : billAmountMap.entrySet()) {
			response.appendOutput("Taking quantity: " + entry.getValue() + " for bill: " + entry.getKey() + "\n");
			try {
				cashRegister.take(entry.getKey(), entry.getValue());
				response.appendOutput("Success\n");
			} catch (CashRegisterException e) {
				response.addError("Take failed for bill: " + entry.getKey() + " Error: " + e.getMessage());
			}
		}
	}
}
