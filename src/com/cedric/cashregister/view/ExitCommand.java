package com.cedric.cashregister.view;

import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;

public class ExitCommand implements CashRegisterCommand {

	@Override
	public CommandResponse execute(String input, CashRegister cashRegister)  {
		CommandResponse response = new CommandResponse();
		response.setOutput("bye");
		response.setShouldTerminate(true);
		return response;
		
	}

}
