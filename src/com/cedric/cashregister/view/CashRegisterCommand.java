package com.cedric.cashregister.view;

import com.cedric.cashregister.CashRegister;
import com.cedric.cashregister.CashRegisterException;

public interface CashRegisterCommand {
	CommandResponse execute(String input, CashRegister cashRegister);
}
