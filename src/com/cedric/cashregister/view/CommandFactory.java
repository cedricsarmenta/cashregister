package com.cedric.cashregister.view;

public class CommandFactory {
	public static CashRegisterCommand getCommandFromInput(String input) {
		input = input.toLowerCase().trim();
		if(input.equals("show")) {
			return new ShowCommand();
		} else if(input.startsWith("put")) {
			return new PutCommand();
		} else if(input.startsWith("quit")) {
			return new ExitCommand();
		}else if(input.startsWith("take")) {
			return new TakeCommand();
		} else if (input.startsWith("change")) {
			return new ChangeCommand();
		}
		throw new IllegalArgumentException("Un-recognized command");
	}
}
